import React from 'react';

import {Redirect, Route, Switch} from "react-router-dom";
import AddCocktail from "./Container/AddCocktail/AddCocktail";
import MainPage from "./Container/MainPage/MainPage";
import UsersCocktails from "./Container/UsersCocktails/UsersCocktails";
import AdminPage from "./Container/AdminPage/AdminPage";
import CocktailInfo from "./Container/CocktailInfo/CocktailInfo";

const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to="/"/>
};

const Routes = ({user}) => {
    return (
        <Switch>
            <ProtectedRoute
                isAllowed={user && user.role === 'admin'}
                path="/admin/"
                exact
                component={() => <AdminPage />}
            />
            <Route path="/" exact component={MainPage}/>
            <Route path="/cocktails/:id" exact component={CocktailInfo}/>
            <Route path="/my_cocktails/:id" exact component={UsersCocktails}/>
            <Route path="/add" exact component={AddCocktail}/>
        </Switch>
    );
};

export default Routes;