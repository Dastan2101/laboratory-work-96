import axios from '../../axios-api';
import {push} from "connected-react-router";

export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';

export const FETCH_COCKTAILS_SUCCESS = 'FETCH_COCKTAILS_SUCCESS';

export const FETCH_USERS_COCKTAILS = 'FETCH_USERS_COCKTAILS';

export const FETCH_ALL_COCKTAILS = 'FETCH_ALL_COCKTAILS';

export const FETCH_COCKTAIL_INFO = 'FETCH_COCKTAIL_INFO';

const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

const fetchCocktailsSuccess = data => ({type: FETCH_COCKTAILS_SUCCESS, data});

const fetchUsersCocktails = data => ({type: FETCH_USERS_COCKTAILS, data});

const fetchAllCocktails = data => ({type: FETCH_ALL_COCKTAILS, data});

const fetchCocktailInfo = data => ({type: FETCH_COCKTAIL_INFO, data});

export const logoutUser = () => {
    return dispatch => {
        return axios.delete('/users/sessions').then(
            () => {
                dispatch({type: LOGOUT_USER});
            }
        )
    }
};


export const facebookLogin = (userData) => {
    return dispatch => {
        axios.post('/users/facebookLogin', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user));
                dispatch(push('/'))
            },
            () => {
                dispatch(loginUserFailure('Login failed!'))
            }
        )
    }
};

export const createCocktail = data => {
    return dispatch => {
        axios.post('/cocktails', data).then(
            dispatch(push('/'))
        )
    }
};

export const getCocktails = () => {
    return dispatch => {
        axios.get('/cocktails').then(
            response => {
                dispatch(fetchCocktailsSuccess(response.data));
            }
        )
    }
};

export const getUsersCocktails = id => {
    return dispatch => {
        axios.get('/cocktails/my_cocktails/' + id).then(
            response => dispatch(fetchUsersCocktails(response.data))
        )
    }
};

export const getAllCocktails = () => {
    return dispatch => {
        axios.get('/admin').then(
            response => dispatch(fetchAllCocktails(response.data))
        )
    }
};

export const cocktailPublication = (id) => {
    return dispatch => {
        axios.post('/admin/publish/' + id).then(
            response => dispatch(fetchAllCocktails(response.data))
        )
    }
};

export const deletePublic = (id) => {
    return dispatch => {
        return axios.delete('/admin/publish/' + id).then(
            response => dispatch(fetchAllCocktails(response.data))
        );
    };
};

export const getCocktailInfo = (id) => {
    return dispatch => {
        axios.get('/cocktails/' + id).then(
            response => dispatch(fetchCocktailInfo(response.data))
        )
    }
};