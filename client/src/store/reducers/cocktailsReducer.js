import {FETCH_COCKTAIL_INFO, FETCH_COCKTAILS_SUCCESS, FETCH_USERS_COCKTAILS} from "../actions/Actions";

const initialState = {
    cocktails: [],
    usersCocktails: [],
    cocktailInfo: null
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COCKTAILS_SUCCESS:
            return {
                ...state, cocktails: action.data
            };
        case FETCH_USERS_COCKTAILS:
            return {
                ...state, usersCocktails: action.data
            };
        case FETCH_COCKTAIL_INFO:
            return {
                ...state, cocktailInfo: action.data
            };

        default:
            return state;
    }
};

export default usersReducer;
