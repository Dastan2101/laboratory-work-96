import {FETCH_ALL_COCKTAILS} from "../actions/Actions";

const initialState = {
    allCocktails: [],
};

const adminReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALL_COCKTAILS:
            return {
                ...state, allCocktails: action.data
            };
        default:
            return state;
    }
};

export default adminReducer;
