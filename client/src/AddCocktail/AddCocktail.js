import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import InputItem from "../Components/InputItem/InputItem";
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';
import IconButton from "@material-ui/core/IconButton/IconButton";

import {connect} from "react-redux";
import {createCocktail} from "../store/actions/Actions";

const styles = theme => ({
    container: {
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'column',
        maxWidth: '50%',
        margin: '0 auto'
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    textField: {
        width: '100%',
        margin: '0 auto 0 0'
    },
    margin: {
        margin: theme.spacing.unit,
    },
});


class AddCocktail extends Component {

    state = {
        title: '',
        recipe: '',
        image: '',
        ingredients: [
            {name: '', quantity: '', key: Math.random().toString()}
        ]
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    addIngredient = () => {
        this.setState({
            ingredients: [
                ...this.state.ingredients,
                {name: '', quantity: '', key: Math.random().toString()}
            ]
        })
    };

    removeIngredient = (idx) => {
        const ingredients = [...this.state.ingredients];
        ingredients.splice(idx, 1);
        console.log(ingredients)
        this.setState({ingredients})
    };

    ingredientInputChangeHandler = (event, idx) => {
        const ingredient = {...this.state.ingredients[idx]};
        ingredient[event.target.name] = event.target.value;

        const ingredients = [...this.state.ingredients];
        ingredients[idx] = ingredient;
        this.setState({ingredients: ingredients})

    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            if (key === 'ingredients') {
                formData.append(key, JSON.stringify(this.state[key]))
            } else formData.append(key, this.state[key]);

        });

        this.props.createCocktail(formData)

    };

    render() {
        const {classes} = this.props;

        return (
            <form className={classes.container} noValidate autoComplete="off" onSubmit={this.submitFormHandler}>
                <InputItem
                    required
                    label="Title"
                    type="text"
                    value={this.state.title}
                    onChange={this.inputChangeHandler}
                    name="title"
                />
                <InputItem
                    label="Recipe"
                    type="text"
                    value={this.state.recipe}
                    onChange={this.inputChangeHandler}
                    name="recipe"
                />
                <input
                    className={classes.input}
                    id="contained-button-file"
                    multiple
                    type="file"
                    name="image"
                    onChange={this.fileChangeHandler}
                />
                {this.state.ingredients.map((item, idx) => (
                    <div key={item.key}>
                        <InputItem
                            label={`Ingredient:${idx + 1}`}
                            type="text"
                            name="name"
                            onChange={(event) => this.ingredientInputChangeHandler(event, idx)}
                        />
                        <InputItem
                            label={`Quantity:${idx + 1}`}
                            type="number"
                            name="quantity"
                            onChange={(event) => this.ingredientInputChangeHandler(event, idx)}
                        />
                        {idx > 0 && <IconButton
                            onClick={() => this.removeIngredient(idx)}>
                            x
                        </IconButton>}
                    </div>
                ))}
                <Button variant="contained" component="span" className={classes.button} onClick={this.addIngredient}>
                    Add ingredient
                </Button>
                <label htmlFor="contained-button-file" style={{display: 'table', margin: '0 auto'}}>
                    <Button variant="contained" component="span" className={classes.button}>
                        Download image
                    </Button>
                </label>
                <Button variant="contained" color="secondary" size="small" className={classes.button}
                        type="submit">
                    <SaveIcon className={classes.leftIcon}/>
                    Save
                </Button>
            </form>
        );
    }
}

const mapStateToProps = state => ({
    artists: false
});

const mapDispatchToProps = dispatch => ({
    createCocktail: data => dispatch(createCocktail(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AddCocktail));