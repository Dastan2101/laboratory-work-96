import React, {Component} from 'react';
import {cocktailPublication, deletePublic, getAllCocktails} from "../../store/actions/Actions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import CardActionArea from "@material-ui/core/CardActionArea/CardActionArea";
import ImageThumbnail from "../../Components/ImageThumbNail/ImageThumbNail";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import Card from "@material-ui/core/Card/Card";
import {withStyles} from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    card: {
        maxWidth: 300,
        marginBottom: 20,
        display: 'inline-block',
        margin: '0 50px'
    },
    media: {
        objectFit: 'curtain',
    },
    root: {
        flexGrow: 1,
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    }

});

class AdminPage extends Component {
    componentDidMount() {
        this.props.getAllCocktails()
    }

    render() {

        let allCocktails;
        const {classes} = this.props;

        if (!this.props.allCocktails) {
            allCocktails = <div>Loading...</div>
        } else {
            allCocktails = this.props.allCocktails.map((item, key) => (
                <Card
                    className={classes.card}
                    key={key}
                >

                    <Link to={'/cocktails/' + item._id} style={{textDecoration: 'none'}}>
                        <CardActionArea>
                            <ImageThumbnail image={item.image}/>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2" color="primary">
                                    Title:
                                    {item.title}
                                </Typography>
                                <Typography gutterBottom variant="h5" component="h2" color="primary">
                                    Published:
                                    {item.published ? <span
                                        style={{color: 'green'}}> Yes </span> : <span style={{color: 'red'}}>No</span>}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                    </Link>
                    {item.published ? <Button variant="contained" color="secondary" className={classes.button}
                                              onClick={() => this.props.deletePublic(item._id)}
                        >
                            Delete
                            <DeleteIcon className={classes.rightIcon}/>
                        </Button> :
                        <Button variant="contained" size="small" className={classes.button}
                                onClick={() => this.props.cocktailPublication(item._id)}
                                style={{backgroundColor: 'green', color: 'white'}}
                        >
                              Publication
                        </Button>}

                </Card>
            ))
        }

        return (
            <div>
                {allCocktails}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    allCocktails: state.admin.allCocktails
});

const mapDispatchToProps = dispatch => ({
    getAllCocktails: () => dispatch(getAllCocktails()),
    cocktailPublication: (id) => dispatch(cocktailPublication(id)),
    deletePublic: (id) => dispatch(deletePublic(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AdminPage));