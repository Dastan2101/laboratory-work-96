import React, {Component} from 'react';
import {connect} from "react-redux";
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {getCocktailInfo} from "../../store/actions/Actions";
import img from '../../assets/notImage.jpg'

const styles = theme => ({
    card: {
        maxWidth: 400,
        maxHeight: 700,
        margin: '0 auto'
    },
    media: {
        paddingTop: '56.25%',
        backgroundSize: 'curtain'
    }
});

class CocktailInfo extends Component {

    componentDidMount() {
        this.props.getCocktailInfo(this.props.match.params.id)
    }


    render() {
        const {classes} = this.props;

        return (
            <div>
                {this.props.cocktailInfo && <Card className={classes.card}>
                    <CardHeader
                        title={'Title'}
                        subheader={this.props.cocktailInfo.title}
                    />
                    <CardMedia
                        className={classes.media}
                        image={
                            this.props.cocktailInfo.image ?
                                'http://localhost:8000/uploads/'
                                + this.props.cocktailInfo.image
                                : img
                        }

                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2" color="primary">
                            Recipe:
                            {this.props.cocktailInfo.recipe}
                        </Typography>
                        <ul >
                            Ingredients:
                            {this.props.cocktailInfo.ingredients.map((ingredient, key) => (
                                <li key={key}>{ingredient.name} : {ingredient.quantity} ml</li>
                            ))}
                        </ul>
                    </CardContent>
                </Card>}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    cocktailInfo: state.cocktails.cocktailInfo
});

const mapDispatchToProps = dispatch => ({
    getCocktailInfo: (id) => dispatch(getCocktailInfo(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(CocktailInfo));