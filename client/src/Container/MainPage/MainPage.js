import React, {Component} from 'react';
import {connect} from "react-redux";
import {getCocktails} from "../../store/actions/Actions";
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import ImageThumbnail from "../../Components/ImageThumbNail/ImageThumbNail";
import {Link} from "react-router-dom";

const styles = {
    card: {
        maxWidth: 300,
        marginBottom: 20,
        display: 'inline-block',
        margin: '0 50px'
    },
    media: {
        objectFit: 'curtain',
    },
    root: {
        flexGrow: 1,
    }

};


class MainPage extends Component {

    componentDidMount() {
        this.props.getCocktails()

    }

    render() {
        const {classes} = this.props;

        return (
            <div>
                {this.props.cocktails.map((item, key) => (
                    <Card
                        className={classes.card}
                        key={key}
                    >

                        <Link to={'/cocktails/' + item._id} style={{textDecoration: 'none'}}>
                            <CardActionArea>
                                <ImageThumbnail image={item.image}/>
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2" color="primary">
                                        Title:
                                        {item.title}
                                    </Typography>
                                </CardContent>

                            </CardActionArea>
                        </Link>

                    </Card>
                ))}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cocktails: state.cocktails.cocktails
})

const mapDispatchToProps = dispatch => ({
    getCocktails: () => dispatch(getCocktails())
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MainPage));