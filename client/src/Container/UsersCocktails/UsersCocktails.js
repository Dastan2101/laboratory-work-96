import React, {Component} from 'react';
import {getUsersCocktails} from "../../store/actions/Actions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {withStyles} from '@material-ui/core/styles';
import CardActionArea from "@material-ui/core/CardActionArea/CardActionArea";
import ImageThumbnail from "../../Components/ImageThumbNail/ImageThumbNail";
import CardContent from "@material-ui/core/CardContent/CardContent";
import Typography from "@material-ui/core/Typography/Typography";
import Card from "@material-ui/core/Card/Card";


const styles = {
    card: {
        maxWidth: 300,
        marginBottom: 20,
        display: 'inline-block',
        margin: '0 50px'
    },
    media: {
        objectFit: 'curtain',
    },
    root: {
        flexGrow: 1,
    }

};

class UsersCocktails extends Component {

    componentDidMount() {
        this.props.getUsersCocktails(this.props.match.params.id)
    }

    render() {
        const {classes} = this.props;

        let list;

        if (!this.props.usersCocktails) {
            list = <div>Loading...</div>
        } else {
            list = this.props.usersCocktails.map((item, key) => (
                <Card
                    className={classes.card}
                    key={key}
                >

                    <Link to={'/cocktails/' + item._id} style={{textDecoration: 'none'}}>
                        <CardActionArea>
                            <ImageThumbnail image={item.image}/>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2" color="primary">
                                    Title:
                                    {item.title}
                                </Typography>
                                <Typography gutterBottom variant="h5" component="h2" color="primary">
                                    Published:
                                    {item.published ? <span
                                        style={{color: 'green'}}> Yes </span> : <span style={{color: 'red'}}>No</span>}
                                </Typography>
                            </CardContent>

                        </CardActionArea>
                    </Link>

                </Card>
            ))
        }


        return (
            <div>
                {list}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    usersCocktails: state.cocktails.usersCocktails
});

const mapDispatchToProps = dispatch => ({
    getUsersCocktails: usersData => dispatch(getUsersCocktails(usersData))
});

export default connect(mapStateToProps,
    mapDispatchToProps)(withStyles(styles)(UsersCocktails));