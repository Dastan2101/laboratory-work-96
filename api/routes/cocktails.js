const express = require('express');
const Cocktails = require('../models/Cocktails');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');

const auth = require('../middleware/auth');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', auth, async (req, res) => {

    try {
        const cocktails = await Cocktails.find({published: true});
        res.send(cocktails)

    } catch (e) {
        res.sendStatus(e)
    }


});

router.get('/my_cocktails/:id', auth, async (req, res) => {

    try {
        const cocktails = await Cocktails.find({user: req.params.id});

        res.send(cocktails)
    } catch (e) {
        res.sendStatus(500)
    }

});

router.get('/:id', auth, async (req, res) => {

    try {
        const cocktail = await Cocktails.findById(req.params.id);
        res.send(cocktail)
    } catch (e) {
        res.sendStatus(500)
    }

});

router.post('/', [auth, upload.single('image')], async (req, res) => {

    try {
        const cocktails = await new Cocktails({
            user: req.user._id,
            title: req.body.title,
            image: req.file ? req.file.filename : null,
            recipe: req.body.recipe,
            ingredients: JSON.parse(req.body.ingredients)

        });

        await cocktails.save();

        res.send(cocktails)

    } catch (error) {
        res.sendStatus(error);
    }

});

module.exports = router;