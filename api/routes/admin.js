const express = require('express');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Cocktails = require('../models/Cocktails');

const router = express.Router();

router.get('/', [auth, permit('admin')], async (req, res) => {

    const cocktails = await Cocktails.find();

    res.send(cocktails);

});

router.post('/publish/:id', [auth, permit('admin')], async (req, res) => {

    try {
        const cocktail = await Cocktails.findById(req.params.id)

        if (cocktail) {
            cocktail.published = true;
            await cocktail.save();
        }

        const cocktails = await Cocktails.find();

        res.send(cocktails)

    } catch (e) {
        res.sendStatus(500)
    }

});

router.delete('/publish/:id', [auth, permit('admin')], async (req, res) => {

    const cocktail = await Cocktails.findById(req.params.id)

    if (cocktail) {
        cocktail.remove();
        await cocktail.save();
    }

    const cocktails = await Cocktails.find();

    res.send(cocktails);

});

module.exports = router;