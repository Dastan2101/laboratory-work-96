const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CocktailSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
      type: String,
      required: true
    },
    image: String,
    recipe: String,
    published: {
        type: Boolean,
        default: false
    },
    ingredients: [
        {name: String, quantity: Number}
    ]

});

const Cocktails = mongoose.model('Cocktails', CocktailSchema);

module.exports = Cocktails;