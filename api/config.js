const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/cocktail',
    mongoOptions: {useNewUrlParser: true, useCreateIndex: true},
    facebook: {
        appId: '714059882342706',
        appSecret: 'aaa98f3416b815f30714084537912643'
    }
};
