const express = require('express');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');

const users = require('./routes/user');
const cocktails = require('./routes/cocktails');
const admin = require('./routes/admin');

const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {

    app.use('/users', users);
    app.use('/cocktails', cocktails);
    app.use('/admin', admin);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });

});

