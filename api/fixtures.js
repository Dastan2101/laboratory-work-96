const nanoid = require('nanoid');

const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/Users');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    await User.create({
            facebookId: '101986774385434',
            username: "Admin Admin",
            password: "123!#lol#!",
            role: "admin",
            token: nanoid(),
            displayName: 'Adminov Admin'
        },
        {
            facebookId: '107402473838254',
            username: "Ivanov Ivan",
            password: "ivan123",
            role: "user",
            token: nanoid(),
            displayName: 'Ivanov Ivan'
        },
        {
            facebookId: '105950107317734',
            username: 'Petrov Petya',
            password: 'petya123',
            role: 'user',
            token: nanoid(),
            displayName: 'Petrov Petya'
        });

    await connection.close();

};

run().catch(error => {
    console.error('Something went', error);
});